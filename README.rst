========================
My Sample Python Project
========================


.. image:: https://img.shields.io/pypi/v/my_sample_python_project.svg
        :target: https://pypi.python.org/pypi/my_sample_python_project

.. image:: https://img.shields.io/travis/emctl/my_sample_python_project.svg
        :target: https://travis-ci.com/emctl/my_sample_python_project

.. image:: https://readthedocs.org/projects/my-sample-python-project/badge/?version=latest
        :target: https://my-sample-python-project.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Testing a super simple python cli application that may grow into something more.


* Free software: MIT license
* Documentation: https://my-sample-python-project.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
